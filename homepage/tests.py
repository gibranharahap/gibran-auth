from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve
from .views import *

class HomeTest(TestCase):
    def test_using_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'home.html')

    def test_url_login(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_url_register(self):
        response = Client().get('/register')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'register.html')
		
    def setUp(self):
        super().setUp()
        self.user = User(username='kelelawar')
        self.user.set_password("tujuhdelapan90")
        self.user.save()
	
    def test_story_using_home_func(self):
        found = resolve('/')
        self.assertEqual(found.func, home)
        
    def test_story_using_index_func2(self):
        found = resolve('/login')
        self.assertEqual(found.func, loginUser)
        
    def test_story_using_index_func3(self):
        found = resolve('/logout')
        self.assertEqual(found.func, logoutUser)
        
    def test_story_using_index_func4(self):
        found = resolve('/register')
        self.assertEqual(found.func, register)
		
    def test_homepage_login(self):
        user = User.objects.create_user('wow', 'gibranharahap@gmail.com', '7890keren')
        user.set_password('7890keren')
        user.save()

        client = Client()
        response = client.get('')

        self.assertIn('login', response.content.decode('utf8'))
        
        logged_in = client.login(username='wow', password='7890keren')
        self.assertTrue(logged_in)

        response = client.get('')
        self.assertIn('wow', response.content.decode('utf8'))

    def test_homepage_logout(self):
        user = User.objects.create_user('wow', 'gibranharahap@gmail.com', '7890keren')
        user.set_password('7890keren')
        user.save()

        client = Client()
        client.login(username='wow', password='7890keren')

        response = client.get('')
        self.assertIn('wow', response.content.decode('utf8'))

        client.logout()

        response = client.get('')
        self.assertIn('login', response.content.decode('utf8'))
