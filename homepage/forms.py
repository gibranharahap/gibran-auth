from django import forms

class UserForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'name':'user'
    }))

    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'name':'pswd'
    }))